package id.uwinfly.app;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import id.uwinfly.app.ws.interfaces.CustomerWsIface;

public class WsCustomerTest extends WsCallTest {
    private CustomerWsIface customerWs;

    @Before
    public void customerWs() {
        setup();
        auth();
        customerWs = wsClient.create(CustomerWsIface.class);
    }

    @Test
    public void getCustomersTest() {
        assertClass(customerWs.gets(), List.class);
    }


}
