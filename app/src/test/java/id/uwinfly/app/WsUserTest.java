package id.uwinfly.app;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.internal.LinkedTreeMap;

import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import id.uwinfly.app.models.User;
import id.uwinfly.app.ws.WsResponse;
import id.uwinfly.app.ws.interfaces.UserWsIface;
import retrofit2.Response;

import static org.junit.Assert.assertTrue;

public class WsUserTest extends WsCallTest {

    private UserWsIface userWs;

    @Before
    public void userWs() {
        setup();
        auth();
        userWs = wsClient.create(UserWsIface.class);
    }

    @Test
    public void getProfileTest() {
        assertClass(userWs.getInfo(), User.class);
    }

    @Test
    public void getOtherProfileTest() {
        User user = new User();
        user.setLocationId(3L);
        user.setFullName("Erry Test");
        user.setUsername("eaz");
        user.setEmail("eaz@gmail.com");
        user.setAddress("hehehe wkwkwk");
        user.setPostalCode("13140");
        user.setPhoneNumber("08132156548");
        user.setIsAdmin(0L);

//        assertClass(userWs.create(user), User.class);
        try {
            Response<WsResponse<User>> userCreate = userWs.create(user).execute();
            if(!userCreate.isSuccessful() && userCreate.errorBody() != null) {
                Gson gson = new Gson();
                Map map = gson.fromJson(userCreate.errorBody().string(), Map.class);
                Map map1 = (Map) map.get("data");
            }
            assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
