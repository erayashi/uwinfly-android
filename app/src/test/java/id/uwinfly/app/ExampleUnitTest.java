package id.uwinfly.app;

import com.google.gson.Gson;
import com.google.gson.JsonParser;

import org.junit.Test;

import java.util.Map;

import id.uwinfly.app.utils.AES;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void  aes() {

        String key = "1234567890123456";
        try {
            AES aes = new AES();
            System.out.println(aes.decrypt("xkCXJ1EFpivkKCiHufCCxhN67D1xfSqLqigiL4Sm9yYDkXSAO12hPlsb9Xbn7quBO19/IL2VHY/YeC7Ke4CBAHzUh3qj3WQoc35zT+jrXorUjk64GZlnsurUT+MsNfpwSWNWdyIjBqFLo5LgAmMEfw==", key));
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertTrue(true);
    }

    @Test
    public void convert() {
        String dec = "tbpYTDNVND4cFqO+zEFI9KogkOlfKHl9c2by2oTmnAOiYrMJWfaA2cUTA1xwW/WhyvjlLhqtyci" +
                "HABAsZJTzJbAu/Vnl5ZRphbzxzNJmGDBquj4mUyYN7A7hZKpUMrZveR9C1JyAHnkR7AUWrRxuKT+YBDCQTMRx" +
                "p8g0SASCzCrZ+S85/vXtqLtcMsVqENWucW2yEXVufL4gMibfqCIxJll3FHenxlFeFJt625ru4lcMOhdT1Nt+N6tv4" +
                "z7s6sJXnC4/50z2Al5Xw37xjlRedKdTto74yYxUl6wJ1RDfrP6Q0ylgK/nEWBwVvEWFKQ6jIFH6VB9XqACei6ojW5n+" +
                "GIgyz0LZUQG2zbhSGhoF7c7ESjiZTDrG9FVmiBODQxPfoEN0tEk5mJS0dRQlFzFmuSHhdH38ANrVi6ECO7zt8XyZ/T" +
                "+8k3ImC46JVU+1r9f+Xpk0YukJGsmmfrg5R2BHY1E1hP6F2uevwdjd4Glmeqw6S1dGi8zWxaUvvDVZBKwGwnXATqunrmjYZO" +
                "RSLm2O3ItnrDUbkx8Y7UxxZucRQ+173Yi37DrHNNJSS/Fiq/0MVv3feQcYWGiySXckXM+5Dg==";
        String key = "eyJpdiI6Iis5MnZ4TlY1djd3WlNWTGhvTzRKMGc9PSIsInZhbHVlIjoiTm5KZlRxWEpBaTI5SzBRN" +
                "01hRW5FU3d3a0xEY2swclV1em82RGpVeVFnNlJ4RnhQZ1ZiTDIwNDBnT0M0dUpRZiIsIm1hYyI6Ijc0ZjZiM2Y0YW" +
                "FjOGEyNmNlNWVmYjQ5MzM1YTYyYWUwYjUxZjc2NTRkNTJhYjM5Y2U0NjRlZmE4N2NhYmIwYzIifQ==";
        try {
            JsonParser jsonParser = new JsonParser();
            jsonParser.parse(dec);
        } catch (Exception e) {
            String aes = null;
            try {
                aes = AES.decrypt(dec, key);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            System.out.println("|" + aes);
        }

        assertTrue(true);
    }
}