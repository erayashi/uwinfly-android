package id.uwinfly.app;


import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Base64;

import id.uwinfly.app.ws.WsClient;
import id.uwinfly.app.ws.WsResponse;
import id.uwinfly.app.ws.WsResponsePage;
import id.uwinfly.app.ws.interfaces.AuthWsIface;
import id.uwinfly.app.ws.responses.AuthResponse;
import retrofit2.Call;
import retrofit2.Response;

import static org.junit.Assert.*;

public class WsCallTest {
    WsClient wsClient;

    @Before
    public void setup() {
        wsClient = new WsClient();
    }

    boolean auth() {
        String basic = Base64.getEncoder().encodeToString(
                ("admin@uwinfly.id:admin123").getBytes()
        ).trim();

        AuthWsIface auth = wsClient.create(AuthWsIface.class);

        try {
            Response<WsResponse<AuthResponse>> authExec = auth.auth("Basic " + basic)
                    .execute();
            if(authExec.isSuccessful())
                wsClient = new WsClient(authExec.body() != null ? authExec.body().getData().getToken() : null);
            return authExec.isSuccessful();
        } catch (IOException e) {
            e.printStackTrace();
            fail();
            return false;
        }
    }

    @Test
    public void authTest() {
       assertTrue(auth());
    }

    void assertClass(Call<?> call, Class<?> tClass) {
        try {
            Response<?> exec = call.execute();
            if(exec.body() != null && exec.isSuccessful()) {
                if (exec.body() instanceof WsResponse)
                    assertSame(((WsResponse) exec.body()).getData().getClass(), tClass);
                else if (exec.body() instanceof WsResponsePage)
                    assertSame(((WsResponsePage) exec.body()).getData().getClass(), tClass);
                else fail();
            } else fail();
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }
}
