package id.uwinfly.app.ws.interfaces;

import java.util.List;

import id.uwinfly.app.models.Report;
import id.uwinfly.app.ws.WsConfig;
import id.uwinfly.app.ws.WsResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ReportWsIface {
    @GET(WsConfig.REPORT)
    public Call<WsResponse<List<Report>>> get();

    @GET(WsConfig.REPORT)
    public Call<WsResponse<List<Report>>> get(@Query("year_month") String yearMonth);
}
