package id.uwinfly.app.ws;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;

import org.json.JSONObject;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import id.uwinfly.app.UwinflyPref;
import id.uwinfly.app.utils.AES;
import okhttp3.ResponseBody;
import retrofit2.Converter;

public class JsonResponseBodyConverter<T> implements Converter<ResponseBody, T> {
    private static String TAG = JsonResponseBodyConverter.class.getSimpleName();
    private final Gson mGson;//gson object
    private final TypeAdapter<T> adapter;
    private final UwinflyPref mpref;

    JsonResponseBodyConverter(Gson mGson, TypeAdapter<T> adapter, UwinflyPref pref) {
        this.mGson = mGson;
        this.adapter = adapter;
        this.mpref = pref;
    }

    @Override
    public T convert(@NonNull ResponseBody value) throws IOException {
        String responseString = value.string();
        String result = responseString;
        String key = mpref.getString(UwinflyPref.Key.STR_TOKEN);
        if(!isJson(responseString) && key != null) {
            try {
                result = AES.decrypt(responseString, key);
            } catch (Exception e) {
                e.printStackTrace();
                result = responseString;
            }
        }

        Log.i(TAG, "key : " + key);
        Log.i(TAG, "normal response : " + responseString);
        Log.i(TAG, "after response : " + result);

        int position = result.lastIndexOf("}");
        int startPos = result.indexOf("{");
        String jsonString = result.substring(startPos,position+1);
        Reader reader = StringToReader(jsonString);
        JsonReader jsonReader = mGson.newJsonReader(reader);

        try {
            return adapter.read(jsonReader);
        } finally {
            reader.close();
            jsonReader.close();
        }

    }

    private Reader StringToReader(String json){
        return new StringReader(json);
    }

    private boolean isJson(String data) {
        try {
            new JSONObject(data);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
