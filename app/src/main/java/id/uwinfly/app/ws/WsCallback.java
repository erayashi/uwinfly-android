package id.uwinfly.app.ws;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import id.uwinfly.app.ui.BaseActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class WsCallback<T> implements Callback<T> {

    private BaseActivity baseActivity;


    protected WsCallback(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        baseActivity.loading(false);
        if (response.isSuccessful()) {
            T responseBody = response.body();
            if(responseBody instanceof WsResponse) {
                if (((WsResponse) responseBody).isError())
                    baseActivity.showSnackBar(((WsResponse) responseBody).getMessage());
                 else onSuccess(responseBody);

            } else onSuccess(response.body());
        } else {
            Gson gson = new Gson();
            if(response.errorBody() != null) {
                try {
                    WsResponse wsResponse = new WsResponse<>();
                    wsResponse = gson.fromJson(response.errorBody().string(), wsResponse.getClass());
                    if(wsResponse.getData() != null)
                        baseActivity.handleBadRequest((Map<String, ArrayList<String>>) wsResponse.getData());
                    else
                        baseActivity.showSnackBar(wsResponse.getMessage());
                } catch (IOException e) {
                    baseActivity.showSnackBar(e.getMessage());
                }
            } else baseActivity.showSnackBar("unknown error");
        }

    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        baseActivity.loading(false);
        baseActivity.snackBarMake(t.getMessage(), Snackbar.LENGTH_LONG).show();
        t.printStackTrace();
    }

    abstract public void onSuccess(T response);
}
