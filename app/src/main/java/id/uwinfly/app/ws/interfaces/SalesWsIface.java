package id.uwinfly.app.ws.interfaces;

import id.uwinfly.app.models.Sales;
import id.uwinfly.app.ws.WsConfig;
import id.uwinfly.app.ws.WsResponse;
import id.uwinfly.app.ws.WsResponsePage;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

@SuppressWarnings("unused")
public interface SalesWsIface {
    @GET(WsConfig.SALES)
    public Call<WsResponsePage<Sales>> gets();

    @GET(WsConfig.SALES)
    public Call<WsResponsePage<Sales>> search(@Query("search") String query);

    @GET(WsConfig.SALES + "/{salesID}")
    public Call<WsResponse<Sales>> get(@Path("salesID") Long salesID);

    @POST(WsConfig.SALES)
    public Call<WsResponse<Sales>> create(@Body Sales sales);

    @PATCH(WsConfig.SALES + "/{salesID}")
    public Call<WsResponse<Sales>> patch(@Path("salesID") Long salesID, @Body Sales sales);

    @DELETE(WsConfig.SALES + "/{salesID}")
    public Call<WsResponse<Sales>> delete(@Path("salesID") Long salesID);
}
