
package id.uwinfly.app.ws.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import id.uwinfly.app.models.User;

@SuppressWarnings("unused")
public class AuthResponse extends User {

    @SerializedName("expired_at")
    private Long expiredAt;
    @Expose
    private String token;

    public Long getExpiredAt() {
        return expiredAt;
    }

    public void setExpiredAt(Long expiredAt) {
        this.expiredAt = expiredAt;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
