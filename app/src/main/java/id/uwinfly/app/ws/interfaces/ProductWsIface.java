package id.uwinfly.app.ws.interfaces;

import id.uwinfly.app.models.Product;
import id.uwinfly.app.ws.WsConfig;
import id.uwinfly.app.ws.WsResponse;
import id.uwinfly.app.ws.WsResponsePage;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ProductWsIface {
    @GET(WsConfig.PRODUCTS)
    public Call<WsResponsePage<Product>> gets();

    @GET(WsConfig.PRODUCTS)
    public Call<WsResponsePage<Product>> search(@Query("search") String query);

    @GET(WsConfig.PRODUCT + "/{productID}")
    public Call<WsResponse<Product>> get(@Path("productID") Long productID);

    @POST(WsConfig.PRODUCT)
    public Call<WsResponse<Product>> create(@Body Product product);

    @PATCH(WsConfig.PRODUCT + "/{productID}")
    public Call<WsResponse<Product>> update(@Path("productID") Long productID, Product product);

    @DELETE(WsConfig.PRODUCT + "/{productID}")
    public Call<WsResponse<Product>> delete(@Path("productID") Long productID);

    @PATCH(WsConfig.PRODUCT + "/{productID}/stock")
    public Call<WsResponse<Product>> updateStock(@Path("productID") Long productID, Product product);
}
