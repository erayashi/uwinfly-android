package id.uwinfly.app.ws.interfaces;

import id.uwinfly.app.models.Customer;
import id.uwinfly.app.ws.WsConfig;
import id.uwinfly.app.ws.WsResponse;
import id.uwinfly.app.ws.WsResponsePage;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CustomerWsIface {
    @GET(WsConfig.CUSTOMERS)
    public Call<WsResponsePage<Customer>> gets();

    @GET(WsConfig.CUSTOMERS)
    public Call<WsResponsePage<Customer>> search(@Query("search") String query);

    @GET(WsConfig.CUSTOMER + "/{customerID}")
    public Call<WsResponse<Customer>> get(@Path("customerID") Long customerID);

    @POST(WsConfig.CUSTOMER)
    public Call<WsResponse<Customer>> create(@Body Customer customer);
}
