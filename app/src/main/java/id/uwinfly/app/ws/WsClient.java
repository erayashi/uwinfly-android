package id.uwinfly.app.ws;

import android.content.Context;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import id.uwinfly.app.BuildConfig;
import id.uwinfly.app.UwinflyPref;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

public class WsClient {

    private Retrofit retrofit;
    private OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
    private String mToken;
    private UwinflyPref uwinflyPref;

    public WsClient() {
        setOkHttpBuilder();
        setRetrofit();
    }

    public WsClient(String token) {
        mToken = token;
        setOkHttpBuilder();
        okHttpBuilder.addInterceptor(chain ->
            chain.proceed (
                chain.request().newBuilder().addHeader("token", token).build()
            )
        );
        setRetrofit();
    }

    public WsClient(Context context, UwinflyPref sharedPref) {

        setOkHttpBuilder();
        this.uwinflyPref = sharedPref;
        int cacheSize = 10 * 1024 * 1024;
        Cache cache = new Cache(context.getCacheDir(), cacheSize);
        okHttpBuilder.cache(cache);

        okHttpBuilder.addInterceptor(chain -> {
            mToken = sharedPref.getString(UwinflyPref.Key.STR_TOKEN);

            Request ori = chain.request();
            String tokenHeader = ori.headers().get("token");
            Request.Builder requestBuilder = ori.newBuilder();

            if(mToken != null && tokenHeader == null)
                requestBuilder.addHeader("token", mToken);

            return chain.proceed(requestBuilder.build());
        });

        setRetrofit();
    }



    private void setOkHttpBuilder() {
        okHttpBuilder.connectTimeout(60, TimeUnit.SECONDS);
        okHttpBuilder.writeTimeout(60, TimeUnit.SECONDS);
        okHttpBuilder.readTimeout(60, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpBuilder.addInterceptor(interceptor);
        }
    }

    private void setRetrofit() {
        retrofit = new Retrofit.Builder()
                .baseUrl(WsConfig.BASE_URL)
                .client(okHttpBuilder.build())
                .addConverterFactory(JsonConverterFactory.create(new Gson(), uwinflyPref))
                .build();
    }

    public WsInterface call() {
        return retrofit.create(WsInterface.class);
    }


    public <T> T create(Class<T> cname) {
        return retrofit.create(cname);
    }
}
