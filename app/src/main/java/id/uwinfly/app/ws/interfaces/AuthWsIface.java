package id.uwinfly.app.ws.interfaces;

import id.uwinfly.app.ws.WsConfig;
import id.uwinfly.app.ws.WsResponse;
import id.uwinfly.app.ws.responses.AuthResponse;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;

@SuppressWarnings("unused")
public interface AuthWsIface {
    @POST(WsConfig.AUTH)
    public Call<WsResponse<AuthResponse>> auth(@Header("Authorization") String basic);
}
