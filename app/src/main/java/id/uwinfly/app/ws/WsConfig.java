package id.uwinfly.app.ws;

public class WsConfig {
    static final String BASE_URL = "http://skdi.online/api/v1/"; // Your Local IP Address

    public static final String AUTH = "auth";
    public static final String USER = "user";
    public static final String SALES = "sales";
    public static final String CUSTOMERS = "customers";
    public static final String CUSTOMER = "customer";
    public static final String PRODUCTS = "products";
    public static final String PRODUCT = "product";
    public static final String ORDERS = "orders";
    public static final String ORDER = "order";
    public static final String REPORT = "report";
    //public static final String API_REGISTER = API_PATH + "/register.php";
}
