package id.uwinfly.app.ws;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import id.uwinfly.app.UwinflyPref;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

public class JsonConverterFactory  extends Converter.Factory {
    private final Gson gson;
    private final UwinflyPref pref;

    public static JsonConverterFactory create() {
        return create(new Gson(), null);
    }

    public static JsonConverterFactory create(Gson gson, UwinflyPref pref) {
        return new JsonConverterFactory(gson, pref);
    }

    private JsonConverterFactory(Gson gson, UwinflyPref pref) {
        if (gson == null) throw new NullPointerException("gson == null");
        this.gson = gson;
        this.pref = pref;
    }

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        TypeAdapter<?> adapter = gson.getAdapter(TypeToken.get(type));
        return new JsonResponseBodyConverter<>(gson, adapter, pref); //response
    }

    @Override
    public Converter<?, RequestBody> requestBodyConverter(Type type, Annotation[] parameterAnnotations, Annotation[] methodAnnotations, Retrofit retrofit) {
        TypeAdapter<?> adapter = gson.getAdapter(TypeToken.get(type));
        return new JsonRequestBodyConverter<>(gson, adapter);
    }
}
