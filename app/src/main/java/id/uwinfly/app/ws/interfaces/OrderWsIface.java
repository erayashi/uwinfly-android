package id.uwinfly.app.ws.interfaces;

import id.uwinfly.app.models.Order;
import id.uwinfly.app.ws.WsConfig;
import id.uwinfly.app.ws.WsResponse;
import id.uwinfly.app.ws.WsResponsePage;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface OrderWsIface {
    @GET(WsConfig.ORDERS)
    public Call<WsResponsePage<Order>> gets();

    @GET(WsConfig.ORDERS)
    public Call<WsResponsePage<Order>> search(@Query("search") String query);

    @GET(WsConfig.ORDER + "/{orderID}")
    public Call<WsResponse<Order>> get(@Path("orderID") Long orderID);

    @POST(WsConfig.ORDER)
    public Call<WsResponse<Order>> create(@Body Order order);

    @DELETE(WsConfig.ORDER + "/{orderID}")
    public Call<WsResponse<Order>> delete(@Path("orderID") Long orderID);
}
