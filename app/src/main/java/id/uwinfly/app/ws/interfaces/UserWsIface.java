package id.uwinfly.app.ws.interfaces;

import id.uwinfly.app.models.User;
import id.uwinfly.app.ws.WsConfig;
import id.uwinfly.app.ws.WsResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

@SuppressWarnings("unused")
public interface UserWsIface {

    @GET(WsConfig.USER)
    public Call<WsResponse<User>> getInfo();

    @POST(WsConfig.USER)
    public Call<WsResponse<User>> create(@Body User user);
}
