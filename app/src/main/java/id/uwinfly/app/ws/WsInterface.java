package id.uwinfly.app.ws;

import id.uwinfly.app.ws.responses.AuthResponse;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface WsInterface {
    @POST(WsConfig.AUTH)
    public Call<WsResponse<AuthResponse>> auth(@Header("Authorization") String basic);
}
