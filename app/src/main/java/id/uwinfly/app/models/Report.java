
package id.uwinfly.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Report {

    @Expose
    private Long id;
    @Expose
    private String location;
    @SerializedName("order_count")
    private Long orderCount;
    @SerializedName("sum_amount_product")
    private Double sumAmountProduct;
    @SerializedName("sum_sales_product")
    private Long sumSalesProduct;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Long getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Long orderCount) {
        this.orderCount = orderCount;
    }

    public Double getSumAmountProduct() {
        return sumAmountProduct;
    }

    public void setSumAmountProduct(Double sumAmountProduct) {
        this.sumAmountProduct = sumAmountProduct;
    }

    public Long getSumSalesProduct() {
        return sumSalesProduct;
    }

    public void setSumSalesProduct(Long sumSalesProduct) {
        this.sumSalesProduct = sumSalesProduct;
    }

}
