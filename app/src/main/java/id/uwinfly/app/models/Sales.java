
package id.uwinfly.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Sales implements Parcelable {

    @Expose
    private String address;
    @Expose
    private Long id;
    @Expose
    private String name;
    @SerializedName("phone_number")
    private String phoneNumber;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.address);
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeString(this.phoneNumber);
    }

    public Sales() {
    }

    protected Sales(Parcel in) {
        this.address = in.readString();
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.name = in.readString();
        this.phoneNumber = in.readString();
    }

    public static final Parcelable.Creator<Sales> CREATOR = new Parcelable.Creator<Sales>() {
        @Override
        public Sales createFromParcel(Parcel source) {
            return new Sales(source);
        }

        @Override
        public Sales[] newArray(int size) {
            return new Sales[size];
        }
    };
}
