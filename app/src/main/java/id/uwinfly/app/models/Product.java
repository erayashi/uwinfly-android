package id.uwinfly.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class Product implements Parcelable {
    @Expose
    private String color;
    @Expose
    private Long id;
    @Expose
    private Double price;
    @Expose
    private Long stock;
    @Expose
    private String type;
    @Expose
    private Long quantity;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Product() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.color);
        dest.writeValue(this.id);
        dest.writeValue(this.price);
        dest.writeValue(this.stock);
        dest.writeString(this.type);
        dest.writeValue(this.quantity);
    }

    protected Product(Parcel in) {
        this.color = in.readString();
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.price = (Double) in.readValue(Double.class.getClassLoader());
        this.stock = (Long) in.readValue(Long.class.getClassLoader());
        this.type = in.readString();
        this.quantity = (Long) in.readValue(Long.class.getClassLoader());
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel source) {
            return new Product(source);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
}
