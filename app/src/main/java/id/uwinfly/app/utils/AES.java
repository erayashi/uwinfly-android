package id.uwinfly.app.utils;

import org.apache.commons.codec.binary.Base64;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES {
    private static String iv = "InitVector";

    public static String encrypt(String clearText, String secretKey) throws Exception {
        return encrypt(clearText, secretKey, iv);
    }

    private static String encrypt(String clearText, String secretKey, String initVector) throws Exception {

        byte[] bytePass = secretKey.getBytes(StandardCharsets.UTF_8);
        byte[] byteV = initVector.getBytes(StandardCharsets.UTF_8);

        byte[] byteKey = Arrays.copyOf(bytePass, 16);
        byte[] byteIV = Arrays.copyOf(byteV, 16);
        // System.out.println(DatatypeConverter.printHexBinary(byteKey));
        // System.out.println(DatatypeConverter.printHexBinary(byteIV));

        SecretKeySpec skeySpec = new SecretKeySpec(byteKey, "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(byteIV);

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivSpec);

        byte[] byteText = clearText.getBytes(StandardCharsets.UTF_8);
        byte[] buf = cipher.doFinal(byteText);

        return Base64.encodeBase64String(buf);
    }

    public static String decrypt(String data, String secretKey) throws Exception {
        return decrypt(data, secretKey, iv);
    }

    private static String decrypt(String data, String secretKey, String initVector) throws Exception {
        byte[] byteData = Base64.decodeBase64(data.getBytes(StandardCharsets.UTF_8));
        byte[] bytePass = secretKey.getBytes(StandardCharsets.UTF_8);
        byte[] byteV = initVector.getBytes(StandardCharsets.UTF_8);

        byte[] byteKey = Arrays.copyOf(bytePass, 16);
        byte[] byteIV = Arrays.copyOf(byteV, 16);

        SecretKeySpec skeySpec = new SecretKeySpec(byteKey, "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(byteIV);

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivSpec);

        byte[] byteText = cipher.doFinal(byteData);

        return new String(byteText);
    }
}
