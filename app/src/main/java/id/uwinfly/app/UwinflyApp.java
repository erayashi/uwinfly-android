package id.uwinfly.app;

import android.app.Application;

import id.uwinfly.app.ws.WsClient;
import id.uwinfly.app.ws.WsInterface;

public class UwinflyApp extends Application {
    private WsInterface wsCall;
    private UwinflyPref sharedPref;
    private WsClient wsClient;

    @Override
    public void onCreate() {
        super.onCreate();
        this.sharedPref = UwinflyPref.getInstance(this);

        this.wsClient = new WsClient(this, sharedPref);

        this.wsCall = wsClient.call();
    }

    public WsInterface getWsCall() {
        return wsCall;
    }

    public WsClient getWsClient() {
        return this.wsClient;
    }

    public UwinflyPref getSharedPref() {
        return sharedPref;
    }
}
