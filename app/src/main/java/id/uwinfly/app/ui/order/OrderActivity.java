package id.uwinfly.app.ui.order;

import android.content.Intent;
import android.view.View;

import id.uwinfly.app.models.Order;
import id.uwinfly.app.ui.ListActivity;
import id.uwinfly.app.ws.WsCallback;
import id.uwinfly.app.ws.WsResponsePage;
import id.uwinfly.app.ws.interfaces.OrderWsIface;

public class OrderActivity extends ListActivity<Order> {

    private OrderItemAdapter oia = new OrderItemAdapter();
    private OrderWsIface orderWs;
    private WsCallback<WsResponsePage<Order>> callback =
            new WsCallback<WsResponsePage<Order>>(this) {
        @Override
        public void onSuccess(WsResponsePage<Order> response) {
            if(response.getData() != null) oia.setOrders(response.getData());
        }
    };

    @Override
    protected void whileCreate() {
        orderWs = createWsCall(OrderWsIface.class);
        getDataFromNetwork();
        setRecyclerViewAdapter(oia);
    }

    @Override
    protected void onFabClick(View v) {
        startActivity(new Intent(this, OrderAddActivity.class));
    }

    @Override
    protected void onSearchSubmit(String s) {
        if(s.isEmpty()) getDataFromNetwork();
        else orderWs.search(s).enqueue(callback);
    }

    private void getDataFromNetwork() {
        orderWs.gets().enqueue(callback);
    }

    @Override
    public void onItemClick(Order item) {

    }
}
