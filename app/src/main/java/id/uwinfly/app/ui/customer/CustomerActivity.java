package id.uwinfly.app.ui.customer;

import android.content.Intent;
import android.view.View;

import id.uwinfly.app.models.Customer;
import id.uwinfly.app.ui.ListActivity;
import id.uwinfly.app.ws.WsCallback;
import id.uwinfly.app.ws.WsResponsePage;
import id.uwinfly.app.ws.interfaces.CustomerWsIface;

public class CustomerActivity extends ListActivity<Customer> {

    public static final int WITH_RETURN_CUSTOMER = 103;

    private final CustomerItemAdapter cia = new CustomerItemAdapter(this);
    private CustomerWsIface customerWs;
    private final WsCallback<WsResponsePage<Customer>> callback =
            new WsCallback<WsResponsePage<Customer>>(this) {
        @Override
        public void onSuccess(WsResponsePage<Customer> response) {
            if(response.getData() != null) cia.setCustomers(response.getData());
        }
    };

    @Override
    protected void whileCreate() {
        customerWs = createWsCall(CustomerWsIface.class);
        getDataFromNetwork();
        setRecyclerViewAdapter(cia);
    }

    @Override
    protected void onFabClick(View v) {
        startActivity(new Intent(this, CustomerAddActivity.class));
    }

    @Override
    protected void onSearchSubmit(String s) {
        if(s.isEmpty()) getDataFromNetwork();
        else customerWs.search(s).enqueue(callback);
    }

    private void getDataFromNetwork() {
        customerWs.gets().enqueue(callback);
    }

    @Override
    public void onItemClick(Customer item) {
        sendAddOrderResult(WITH_RETURN_CUSTOMER, item);
    }
}
