package id.uwinfly.app.ui.report;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.uwinfly.app.R;
import id.uwinfly.app.models.Report;

public class ReportItemAdapter extends RecyclerView.Adapter<ReportItemAdapter.ReportItemViewHolder> {

    private final List<Report> reports = new ArrayList<>();

    @NonNull
    @Override
    public ReportItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View v = layoutInflater.inflate(R.layout.item_report, viewGroup, false);
        return new ReportItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportItemViewHolder reportItemViewHolder, int i) {
        Report report = reports.get(i);
        if(report != null) reportItemViewHolder.setWithReport(report);
    }

    @Override
    public int getItemCount() {
        return reports.size();
    }

    void setReports(List<Report> reports) {
        this.reports.clear();
        this.reports.addAll(reports);
        notifyDataSetChanged();
    }

    class ReportItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvLocation, tvTotalOrder, tvTotalOrderProduct, tvAmountOrder;
        private String priceFormat, totalOrderFormat, totalOrderProductFormat;
        ReportItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvLocation = itemView.findViewById(R.id.tv_location_item_report);
            tvTotalOrder = itemView.findViewById(R.id.tv_count_order_item_report);
            tvTotalOrderProduct = itemView.findViewById(R.id.tv_sold_product_item_report);
            tvAmountOrder = itemView.findViewById(R.id.tv_amount_item_report);

            priceFormat = itemView.getResources().getString(R.string.price_format);
            totalOrderFormat = itemView.getResources().getString(R.string.total_order_format);
            totalOrderProductFormat = itemView.getResources().getString(R.string.total_order_product_format);
        }

        void setWithReport(Report report) {
            tvLocation.setText(report.getLocation());
            tvTotalOrder.setText(String.format(totalOrderFormat, report.getOrderCount()));
            tvTotalOrderProduct.setText(String.format(totalOrderProductFormat, report.getSumSalesProduct()));
            tvAmountOrder.setText(String.format(priceFormat, report.getSumAmountProduct()));
        }
    }
}
