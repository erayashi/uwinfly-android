package id.uwinfly.app.ui;

import android.annotation.SuppressLint;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import id.uwinfly.app.UwinflyApp;
import id.uwinfly.app.UwinflyPref;

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {

    protected ProgressBar progressBar;
    protected Button button;

    public void loading(boolean isShow) {
        if(progressBar != null) progressBar.setVisibility(isShow ? View.VISIBLE : View.GONE);
        if(button != null) button.setVisibility(isShow ? View.GONE : View.VISIBLE);
    }

    protected Map<String, TextInputLayout> validateFields = new HashMap<>();

    protected UwinflyApp getApp() {
        return (UwinflyApp) getApplication();
    }

    public void showToast(CharSequence text, int duration) {
        Toast.makeText(this, text, duration).show();
    }

    protected UwinflyPref getSharedPref() {
        return getApp().getSharedPref();
    }

    protected boolean isAdmin() {
        return getSharedPref().getLong(UwinflyPref.Key.ID_ADMIN) == 1;
    }

    protected String getTilText(TextInputLayout til){
        return Objects.requireNonNull(til.getEditText()).getText().toString();
    }

    protected <T> T createWsCall(Class<T> tClass) {
        return getApp().getWsClient().create(tClass);
    }


    public void showSnackBar(CharSequence message) {
        snackBarMake(message, Snackbar.LENGTH_SHORT).show();
    }

    public Snackbar snackBarMake(CharSequence message, int duration) {
        View view = getWindow().getDecorView().findViewById(android.R.id.content);
        return Snackbar.make(view, message, duration);
    }

    public Double getTilDouble(TextInputLayout til) {
        String tilString = getTilText(til);
        if(!tilString.isEmpty()) return Double.parseDouble(tilString);
        return null;
    }

    public Long getTilLong(TextInputLayout til) {
        String tilString = getTilText(til);
        if(!tilString.isEmpty()) return Long.parseLong(tilString);
        return null;
    }

    protected void watchTil(TextInputLayout til) {
        Objects.requireNonNull(til.getEditText()).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                til.setError(null);
            }
        });
    }

    public void handleBadRequest(Map<String, ArrayList<String>> invalids) {
        if(invalids != null) {
            int i = 0;
            for (Map.Entry<String, ArrayList<String>> invalid: invalids.entrySet()) {
                TextInputLayout til = validateFields.get(invalid.getKey());
                if(til != null) {
                    til.setErrorEnabled(true);
                    til.setError(TextUtils.join("\n", invalid.getValue()));
                    if(i == 0) til.requestFocus();
                    i++;
                }
            }
        }
    }
}
