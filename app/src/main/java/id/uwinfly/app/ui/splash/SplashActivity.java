package id.uwinfly.app.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;

import id.uwinfly.app.R;
import id.uwinfly.app.UwinflyPref;
import id.uwinfly.app.models.User;
import id.uwinfly.app.ui.BaseActivity;
import id.uwinfly.app.ui.main.LoginActivity;
import id.uwinfly.app.ui.menu.MenuActivity;
import id.uwinfly.app.ws.WsResponse;
import id.uwinfly.app.ws.interfaces.UserWsIface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(this::checkLogin, 1500L);
    }

    protected void checkLogin() {
        if(getSharedPref().getString(UwinflyPref.Key.STR_TOKEN) != null) {
            createWsCall(UserWsIface.class).getInfo().enqueue(new Callback<WsResponse<User>>() {
                @Override
                public void onResponse(@NonNull Call<WsResponse<User>> call,
                                       @NonNull Response<WsResponse<User>> response) {
                    if(response.isSuccessful()) toMenu();
                    else toLogin();
                }

                @Override
                public void onFailure(@NonNull Call<WsResponse<User>> call,
                                      @NonNull Throwable t) {
                    toLogin();
                }
            });
        } else {
            toLogin();
        }
    }

    private void toLogin() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    private void toMenu() {
        startActivity(new Intent(this, MenuActivity.class));
        finish();
    }
}
