package id.uwinfly.app.ui.sales;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.widget.Toast;

import id.uwinfly.app.R;
import id.uwinfly.app.models.Sales;
import id.uwinfly.app.ui.BaseActivity;
import id.uwinfly.app.ws.WsCallback;
import id.uwinfly.app.ws.WsResponse;
import id.uwinfly.app.ws.interfaces.SalesWsIface;

public class SalesAddActivity extends BaseActivity {

    private TextInputLayout tilName, tilAddress, tilPhone;
    private SalesWsIface salesWs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_add);

        tilName = findViewById(R.id.til_sales_name);
        tilAddress = findViewById(R.id.til_sales_address);
        tilPhone = findViewById(R.id.til_sales_phone);
        progressBar = findViewById(R.id.pb_sales_submit);
        button = findViewById(R.id.btn_sales_submit);

        watchTil(tilName);
        watchTil(tilAddress);
        watchTil(tilPhone);
        button.setOnClickListener(l -> onSubmit());

        salesWs = createWsCall(SalesWsIface.class);

        validateFields.put("name", tilName);
        validateFields.put("address", tilAddress);
        validateFields.put("phone_number", tilPhone);
    }

    private void onSubmit() {
        Sales sales = new Sales();
        sales.setName(getTilText(tilName));
        sales.setAddress(getTilText(tilAddress));
        sales.setPhoneNumber(getTilText(tilPhone));

        loading(true);

        salesWs.create(sales).enqueue(new WsCallback<WsResponse<Sales>>(this) {
            @Override
            public void onSuccess(WsResponse<Sales> response) {
                showToast("Tambah produk berhasil", Toast.LENGTH_LONG);
                finish();
            }
        });
    }
}
