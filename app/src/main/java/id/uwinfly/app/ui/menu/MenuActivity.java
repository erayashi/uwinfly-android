package id.uwinfly.app.ui.menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import id.uwinfly.app.R;
import id.uwinfly.app.UwinflyPref;
import id.uwinfly.app.models.User;
import id.uwinfly.app.ui.BaseActivity;
import id.uwinfly.app.ui.customer.CustomerActivity;
import id.uwinfly.app.ui.main.LoginActivity;
import id.uwinfly.app.ui.order.OrderActivity;
import id.uwinfly.app.ui.product.ProductActivity;
import id.uwinfly.app.ui.report.ReportActivity;
import id.uwinfly.app.ui.sales.SalesActivity;
import id.uwinfly.app.ws.WsCallback;
import id.uwinfly.app.ws.WsResponse;
import id.uwinfly.app.ws.interfaces.UserWsIface;

public class MenuActivity extends BaseActivity {
    private TextView tvUsername;
    private CardView mOrder, mCustomer, mProduct, mSales, mReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        tvUsername = findViewById(R.id.menu_lbl_username);

        mOrder = findViewById(R.id.menu_order);
        mOrder.setOnClickListener(l -> toOrder());
        mCustomer = findViewById(R.id.menu_customer);
        mCustomer.setOnClickListener(l -> toCustomer());
        mProduct = findViewById(R.id.menu_product);
        mProduct.setOnClickListener(l -> toProduct());
        mSales = findViewById(R.id.menu_sales);
        mSales.setOnClickListener(l -> toSales());
        mReport = findViewById(R.id.menu_report);
        mReport.setOnClickListener(l -> toReport());

        findViewById(R.id.btn_logout).setOnClickListener(l -> logout());
        tvUsername.setOnClickListener(l -> toBase());

        getProfile();
    }

    private void getProfile() {
        long isAdmin = getSharedPref().getLong(UwinflyPref.Key.ID_ADMIN);
        Log.d(MenuActivity.class.getSimpleName(), "getProfile admin : " + isAdmin);
        if(isAdmin == 1) {
            mOrder.setVisibility(View.GONE);
            mCustomer.setVisibility(View.GONE);
        } else {
            mReport.setVisibility(View.GONE);
            mProduct.setVisibility(View.GONE);
            mSales.setVisibility(View.GONE);
        }
        createWsCall(UserWsIface.class).getInfo().enqueue(
        new WsCallback<WsResponse<User>>(this) {
            @Override
            public void onSuccess(WsResponse<User> response) {
                tvUsername.setText(response.getData().getFullName());
            }
        });
    }

    private void toCustomer() {
        startActivity(new Intent(this, CustomerActivity.class));
    }

    private void toProduct() {
        startActivity(new Intent(this, ProductActivity.class));
    }

    private void toOrder() {
        startActivity(new Intent(this, OrderActivity.class));
    }

    private void toSales() {
        startActivity(new Intent(this, SalesActivity.class));
    }

    private void toReport() {
        startActivity(new Intent(this, ReportActivity.class));
    }

    private void toBase() {
        startActivity(new Intent(this, id.uwinfly.app.ui.test.BaseActivity.class));
    }

    private void logout() {
        getSharedPref().clear();
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }
}
