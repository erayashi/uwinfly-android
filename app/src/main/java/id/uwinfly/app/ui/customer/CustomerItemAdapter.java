package id.uwinfly.app.ui.customer;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.uwinfly.app.R;
import id.uwinfly.app.models.Customer;
import id.uwinfly.app.ui.ItemCallback;

public class CustomerItemAdapter extends
        RecyclerView.Adapter<CustomerItemAdapter.CustomerItemViewHolder> {

    private List<Customer> customers = new ArrayList<>();
    private ItemCallback<Customer> itemCallback;

    CustomerItemAdapter(ItemCallback<Customer> itemCallback) {
        this.itemCallback = itemCallback;
    }

    @NonNull
    @Override
    public CustomerItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v = inflater.inflate(R.layout.item_customer, viewGroup, false);
        return new CustomerItemViewHolder(v, itemCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomerItemViewHolder customerItemViewHolder, int i) {
        Customer customer = customers.get(i);
        if(customer != null) customerItemViewHolder.setWithCustomer(customer);
    }

    @Override
    public int getItemCount() {
        if(customers == null) return 0;
        return customers.size();
    }

    void setCustomers(List<Customer> customers) {
        this.customers = customers;
        notifyDataSetChanged();
    }

    class CustomerItemViewHolder extends RecyclerView.ViewHolder {
        TextView name, address, phone;
        Customer mCustomer;

        CustomerItemViewHolder(@NonNull View itemView, ItemCallback<Customer> itemCallback) {
            super(itemView);
            name = itemView.findViewById(R.id.icustomer_name);
            address = itemView.findViewById(R.id.icustomer_address);
            phone = itemView.findViewById(R.id.icustomer_phone);

            itemView.setOnClickListener(l -> itemCallback.onItemClick(mCustomer));
        }

        void setWithCustomer(Customer customer) {
            mCustomer = customer;
            name.setText(customer.getName());
            address.setText(customer.getAddress());
            phone.setText(customer.getPhoneNumber());
        }
    }
}
