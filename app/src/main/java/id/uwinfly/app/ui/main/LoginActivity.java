package id.uwinfly.app.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Base64;

import id.uwinfly.app.R;
import id.uwinfly.app.ui.BaseActivity;
import id.uwinfly.app.ui.menu.MenuActivity;
import id.uwinfly.app.ws.WsCallback;
import id.uwinfly.app.ws.WsResponse;
import id.uwinfly.app.ws.interfaces.AuthWsIface;
import id.uwinfly.app.ws.responses.AuthResponse;

public class LoginActivity extends BaseActivity {
    private TextInputLayout tilEmail, tilPwd;
    private String email, pwd;
    private boolean isValid = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tilEmail = findViewById(R.id.til_email);
        watchTil(tilEmail);

        tilPwd = findViewById(R.id.til_password);
        watchTil(tilPwd);

        button = findViewById(R.id.btn_login);
        progressBar = findViewById(R.id.pb_login);

        button.setOnClickListener(v -> postLogin());
    }

    private void validateForm() {
        isValid = true;

        email = getTilText(tilEmail);
        pwd = getTilText(tilPwd);

        if(email.isEmpty()) {
            tilEmail.setError("email harus di isi");
            tilEmail.setFocusable(true);
            isValid = false;
        }

        if(pwd.isEmpty()) {
            tilPwd.setError("password harus di isi");
            tilPwd.setFocusable(true);
            isValid = false;
        }
    }

    private void postLogin() {
        validateForm();

        if(isValid) {
            loading(true);

            String auth = Base64.encodeToString((email + ":" + pwd).getBytes(), Base64.DEFAULT)
                    .trim();

            createWsCall(AuthWsIface.class).auth("Basic " + auth).
                    enqueue(new WsCallback<WsResponse<AuthResponse>>(this) {
                @Override
                public void onSuccess(WsResponse<AuthResponse> response) {
                    loading(false);
                    AuthResponse authResponse = response.getData();
                    getSharedPref().setAfterLogin(authResponse);
                    toMenu();
                }
            });
        }
    }

    private void toMenu() {
        startActivity(new Intent(this, MenuActivity.class));
        finish();
    }

}
