package id.uwinfly.app.ui.order;

import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.button.MaterialButton;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.uwinfly.app.R;
import id.uwinfly.app.models.Product;

public class OrderAddProductAdapter extends
        RecyclerView.Adapter<OrderAddProductAdapter.OrderAddProductViewHolder> implements
        OrderProductCallback {

    private final List<Product> products = new ArrayList<>();
    private final OrderAddCallback addCallback;

    OrderAddProductAdapter(OrderAddCallback addCallback) {
        this.addCallback = addCallback;
    }

    @NonNull
    @Override
    public OrderAddProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.item_product, viewGroup, false);
        return new OrderAddProductViewHolder(v, context, this);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderAddProductViewHolder orderAddProductViewHolder, int i) {
        Product product = products.get(i);
        if(product != null) orderAddProductViewHolder.setWithProduct(product);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    void addProduct(Product product) {
        this.products.add(product);
        notifyItemInserted(products.size());
    }

    List<Product> getProducts() {
        return products;
    }

    boolean isAlreadyAdd(Long productID) {
        for (Product p: getProducts()) {
            if(p.getId().equals(productID)) return true;
        }

        return false;
    }

    private int findKeys(Long id) {
        int i = 0;
        for (Product p: getProducts()) {
            if(p.getId().equals(id)) return i;
            i++;
        }
        return i;
    }

    @Override
    public void remove(Long id) {
        int key = findKeys(id);
        this.products.remove(key);
        notifyItemRemoved(key);
        addCallback.notifyWhenRemoved();
    }

    @Override
    public void changeQuantity(Long id, Long qty) {
        int key = findKeys(id);
        Product product = products.get(key);
        if(product != null) {
            product.setQuantity(qty);
            products.set(key, product);
            notifyItemChanged(key);
        }
    }

    class OrderAddProductViewHolder extends RecyclerView.ViewHolder {
        TextView type, color, price, stock;
        String stockFormat, priceFormat, dialogTitleFormat;
        MaterialButton btnQty, btnRemove;
        Product mProduct;
        Context mContext;
        OrderProductCallback mOrderProductCallback;

        OrderAddProductViewHolder(@NonNull View itemView, Context context,
                                  OrderProductCallback orderProductCallback) {
            super(itemView);
            mContext = context;
            mOrderProductCallback = orderProductCallback;

            type = itemView.findViewById(R.id.iproduct_type);
            color = itemView.findViewById(R.id.iproduct_color);
            price = itemView.findViewById(R.id.iproduct_price);
            stock = itemView.findViewById(R.id.iproduct_stock);

            btnQty = itemView.findViewById(R.id.btn_qty_item_product);
            btnRemove = itemView.findViewById(R.id.btn_remove_item_product);

            itemView.findViewById(R.id.cl_btn_layout_item_product).setVisibility(View.VISIBLE);


            stockFormat = itemView.getContext().getString(R.string.stock_format);
            priceFormat = itemView.getContext().getString(R.string.price_format);
            dialogTitleFormat = itemView.getContext().getString(R.string.dialog_title_format);
        }

        void setWithProduct(Product product) {
            mProduct = product;
            stock.setText(String.format(stockFormat, product.getQuantity()));
            price.setText(String.format(priceFormat, product.getPrice()));
            color.setText(product.getColor());
            type.setText(product.getType());
            btnQty.setOnClickListener(l ->
                showInputDialog(
                    mContext,
                    String.format(dialogTitleFormat, product.getType()),
                    mOrderProductCallback
                )
            );

            btnRemove.setOnClickListener(l -> mOrderProductCallback.remove(mProduct.getId()));
        }

        void showInputDialog(Context context, String title, OrderProductCallback orderProductCallback) {
            final EditText editText = new EditText(context);
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);

            AlertDialog alert = new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setView(editText)
                    .setNegativeButton("Cancel", (dialog, which) -> dialog.cancel())
                    .setPositiveButton("Change Quantity", null).create();

            alert.setOnShowListener(dialog -> {
                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(l -> {
                    String qtyString = editText.getText().toString();
                    if(qtyString.isEmpty()) editText.setError("Quantity is Required");
                    else {
                        Long qty = Long.parseLong(editText.getText().toString());
                        if(qty > mProduct.getStock()) editText.setError("Stock is Not Available");
                        else {
                            orderProductCallback.changeQuantity(mProduct.getId(), qty);
                            dialog.dismiss();
                        }
                    }
                });
            });

            alert.show();
        }
    }


}
