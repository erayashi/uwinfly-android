package id.uwinfly.app.ui.order;

interface OrderProductCallback {
    void remove(Long id);
    void changeQuantity(Long id, Long qty);
}