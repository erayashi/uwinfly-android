package id.uwinfly.app.ui.product;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.widget.Toast;

import id.uwinfly.app.R;
import id.uwinfly.app.models.Product;
import id.uwinfly.app.ui.BaseActivity;
import id.uwinfly.app.ws.WsCallback;
import id.uwinfly.app.ws.WsResponse;
import id.uwinfly.app.ws.interfaces.ProductWsIface;

public class ProductAddActivity extends BaseActivity {

    private TextInputLayout tilType, tilColor, tilPrice, tilStock;
    private ProductWsIface productWs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_add);

        tilColor = findViewById(R.id.til_product_color);
        tilPrice = findViewById(R.id.til_product_price);
        tilType = findViewById(R.id.til_product_type);
        tilStock = findViewById(R.id.til_product_stock);
        progressBar = findViewById(R.id.pb_product_submit);
        button = findViewById(R.id.btn_product_submit);

        button.setOnClickListener(view -> onSubmit());
        watchTil(tilColor);
        watchTil(tilPrice);
        watchTil(tilType);
        watchTil(tilStock);

        productWs = createWsCall(ProductWsIface.class);

        validateFields.put("type", tilType);
        validateFields.put("color", tilColor);
        validateFields.put("price", tilPrice);
        validateFields.put("stock", tilStock);
    }

    private void onSubmit() {
        Product product = new Product();
        product.setPrice(getTilDouble(tilPrice));
        product.setStock(getTilLong(tilStock));
        product.setType(getTilText(tilType));
        product.setColor(getTilText(tilColor));

        loading(true);

        productWs.create(product).enqueue(new WsCallback<WsResponse<Product>>(this) {
            @Override
            public void onSuccess(WsResponse<Product> response) {
                showToast("Tambah produk berhasil", Toast.LENGTH_LONG);
                finish();
            }
        });
    }
}
