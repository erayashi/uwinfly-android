package id.uwinfly.app.ui.order;

public interface OrderAddCallback {
    void notifyWhenRemoved();
}
