package id.uwinfly.app.ui;

public interface ItemCallback<T> {
    void onItemClick(T item);
}
