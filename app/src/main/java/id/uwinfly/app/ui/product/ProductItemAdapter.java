package id.uwinfly.app.ui.product;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.uwinfly.app.R;
import id.uwinfly.app.models.Product;
import id.uwinfly.app.ui.ItemCallback;

public class ProductItemAdapter extends
        RecyclerView.Adapter<ProductItemAdapter.ProductItemViewHolder> {

    private List<Product> products = new ArrayList<>();
    private ItemCallback<Product> itemCallback;

    ProductItemAdapter(ItemCallback<Product> itemCallback) {
        this.itemCallback = itemCallback;
    }

    @NonNull
    @Override
    public ProductItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v = inflater.inflate(R.layout.item_product, viewGroup, false);

        return new ProductItemViewHolder(v, itemCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductItemViewHolder vh, int i) {
        Product product = products.get(i);
        if(product != null) vh.setWithProduct(product);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    void setProducts(List<Product> products) {
        this.products = products;
        notifyDataSetChanged();
    }

    class ProductItemViewHolder extends RecyclerView.ViewHolder {
        TextView type, color, price, stock;
        String stockFormat, priceFormat;
        Product mProduct;

        ProductItemViewHolder(@NonNull View itemView, ItemCallback<Product> itemCallback) {
            super(itemView);
            type = itemView.findViewById(R.id.iproduct_type);
            color = itemView.findViewById(R.id.iproduct_color);
            price = itemView.findViewById(R.id.iproduct_price);
            stock = itemView.findViewById(R.id.iproduct_stock);

            stockFormat = itemView.getContext().getString(R.string.stock_format);
            priceFormat = itemView.getContext().getString(R.string.price_format);

            itemView.setOnClickListener(l -> itemCallback.onItemClick(mProduct));
        }

        void setWithProduct(Product product) {
            mProduct = product;
            stock.setText(String.format(stockFormat, product.getStock()));
            price.setText(String.format(priceFormat, product.getPrice()));
            color.setText(product.getColor());
            type.setText(product.getType());
        }
    }
}
