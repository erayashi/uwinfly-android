package id.uwinfly.app.ui.product;

import android.app.AlertDialog;
import android.content.Intent;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import id.uwinfly.app.R;
import id.uwinfly.app.models.Product;
import id.uwinfly.app.ui.ListActivity;
import id.uwinfly.app.ws.WsCallback;
import id.uwinfly.app.ws.WsResponsePage;
import id.uwinfly.app.ws.interfaces.ProductWsIface;

public class ProductActivity extends ListActivity<Product> {

    public static final int WITH_RETURN_PRODUCT = 101;

    private String dialogTitleFormat;
    private final ProductItemAdapter pia = new ProductItemAdapter(this);
    private ProductWsIface productWs;
    private final WsCallback<WsResponsePage<Product>> callback =
            new WsCallback<WsResponsePage<Product>>(this) {
        @Override
        public void onSuccess(WsResponsePage<Product> response) {
            if(response.getData() != null) pia.setProducts(response.getData());
        }
    };

    @Override
    protected void whileCreate() {
        productWs = createWsCall(ProductWsIface.class);
        getDataFromNetwork();
        setRecyclerViewAdapter(pia);
        dialogTitleFormat = getResources().getString(R.string.dialog_title_format);
    }

    @Override
    protected void onFabClick(View v) {
        startActivity(new Intent(this, ProductAddActivity.class));
    }

    @Override
    protected void onSearchSubmit(String s) {
        if(s.isEmpty()) getDataFromNetwork();
        else productWs.search(s).enqueue(callback);
    }

    private void getDataFromNetwork() {
        productWs.gets().enqueue(callback);
    }

    @Override
    public void onItemClick(Product item) {
        showInputDialog(String.format(dialogTitleFormat, item.getType()), item, quantity -> {
            item.setQuantity(quantity);
            sendAddOrderResult(WITH_RETURN_PRODUCT, item);
        });
    }

    void showInputDialog(String title, Product item, ProductDialogCallback productDialogCallback) {
        final EditText editText = new EditText(this);
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);

        AlertDialog alert = new AlertDialog.Builder(this)
        .setTitle(title)
        .setView(editText)
        .setNegativeButton("Cancel", (dialog, which) -> dialog.cancel())
        .setPositiveButton("Add to Order", null).create();

        alert.setOnShowListener(dialog -> {
            Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(l -> {
                String qtyString = editText.getText().toString();
                if(qtyString.isEmpty()) editText.setError("Quantity is Required");
                else {
                    Long qty = Long.parseLong(editText.getText().toString());
                    if(qty > item.getStock()) editText.setError("Stock is Not Available");
                    else {
                        productDialogCallback.onGetInput(qty);
                        dialog.dismiss();
                    }
                }
            });
        });

        alert.show();
    }

    interface ProductDialogCallback {
        void onGetInput(Long quantity);
    }
}
