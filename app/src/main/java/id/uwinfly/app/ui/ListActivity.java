package id.uwinfly.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.SearchView;

import id.uwinfly.app.R;
import id.uwinfly.app.ui.order.OrderAddActivity;

abstract public class ListActivity<T> extends BaseActivity implements ItemCallback<T> {
    protected FloatingActionButton fab;
    protected RecyclerView rv;
    protected CardView cv;
    protected SearchView sv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        fab = findViewById(R.id.fab);
        rv = findViewById(R.id.recycler_list);
        cv = findViewById(R.id.card_search);
        sv = findViewById(R.id.search_view);

        listener();

        whileCreate();

        fab.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        whileCreate();
    }

    protected abstract void whileCreate();
    protected abstract void onFabClick(View v);
    protected abstract void onSearchSubmit(String s);

    protected void setRecyclerViewAdapter(RecyclerView.Adapter adapter) {
        rv.setAdapter(adapter);
    }

    private void listener() {
        if(isAdmin())
            rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (dy > 0 && fab.getVisibility() == View.VISIBLE) {
                        fab.hide();
                    } else if (dy < 0 && fab.getVisibility() != View.VISIBLE) {
                        fab.show();
                    }
                }
            });
        else fab.hide();

        cv.setOnClickListener(l -> {
            sv.setIconified(false);
            sv.requestFocusFromTouch();
        });

        fab.setOnClickListener(this::onFabClick);

        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                onSearchSubmit(s);
                return false;
            }
        });
    }

    protected void sendAddOrderResult(int requestCode, Parcelable data) {
        if(getIntent() != null &&
                getIntent().getIntExtra(OrderAddActivity.FROM_ORDER, 0) == requestCode) {
            Intent customerIntent = new Intent();
            customerIntent.putExtra(OrderAddActivity.TO_ORDER, data);
            setResult(RESULT_OK, customerIntent);
            finish();
        }
    }
}
