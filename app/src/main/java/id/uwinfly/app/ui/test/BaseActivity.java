package id.uwinfly.app.ui.test;

import android.os.Bundle;
import android.support.design.button.MaterialButton;
import android.support.design.widget.TextInputLayout;
import android.widget.TextView;

import id.uwinfly.app.R;
import id.uwinfly.app.utils.AES;

public class BaseActivity extends id.uwinfly.app.ui.BaseActivity {

    TextInputLayout tilKey, tilData;
    MaterialButton btnEnc, btnDec;
    TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        tilKey = findViewById(R.id.til_base_key);
        tilData = findViewById(R.id.til_base_data);
        btnEnc = findViewById(R.id.btn_enc);
        btnDec = findViewById(R.id.btn_dec);
        tvResult = findViewById(R.id.tv_base_result);
        btnDec.setOnClickListener(v -> crypt(false));
        btnEnc.setOnClickListener(v -> crypt(true));
    }

    private void crypt(boolean encrypt) {
        String key = getTilText(tilKey);
        String value = getTilText(tilData);
        try {
            AES aes = new AES();
            if(encrypt) tvResult.setText(aes.encrypt(value, key));
            else tvResult.setText(aes.decrypt(value, key));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
