package id.uwinfly.app.ui.customer;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.widget.Toast;

import id.uwinfly.app.R;
import id.uwinfly.app.models.Customer;
import id.uwinfly.app.ui.BaseActivity;
import id.uwinfly.app.ws.WsCallback;
import id.uwinfly.app.ws.WsResponse;
import id.uwinfly.app.ws.interfaces.CustomerWsIface;

public class CustomerAddActivity extends BaseActivity {

    private TextInputLayout tilName, tilAddress, tilPhone, tilEmail;
    private CustomerWsIface customerWs;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_add);

        customerWs = createWsCall(CustomerWsIface.class);

        tilName = findViewById(R.id.til_customer_name);
        tilAddress = findViewById(R.id.til_customer_address);
        tilPhone = findViewById(R.id.til_customer_phone);
        tilEmail = findViewById(R.id.til_customer_email);
        progressBar = findViewById(R.id.pb_customer_submit);
        button = findViewById(R.id.btn_customer_submit);

        watchTil(tilName);
        watchTil(tilAddress);
        watchTil(tilPhone);
        watchTil(tilEmail);
        button.setOnClickListener(l -> onSubmit());

        validateFields.put("name", tilName);
        validateFields.put("address", tilAddress);
        validateFields.put("phone_number", tilPhone);
        validateFields.put("email", tilEmail);
    }
    
    private void onSubmit() {
        Customer customer = new Customer();
        customer.setName(getTilText(tilName));
        customer.setAddress(getTilText(tilAddress));
        customer.setEmail(getTilText(tilEmail));
        customer.setPhoneNumber(getTilText(tilPhone));

        loading(true);

        customerWs.create(customer).enqueue(new WsCallback<WsResponse<Customer>>(this) {
            @Override
            public void onSuccess(WsResponse<Customer> response) {
                showToast("Tambah produk berhasil", Toast.LENGTH_LONG);
                finish();
            }
        });
    }
}
