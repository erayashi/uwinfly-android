package id.uwinfly.app.ui.report;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import id.uwinfly.app.R;
import id.uwinfly.app.models.Report;
import id.uwinfly.app.ui.BaseActivity;
import id.uwinfly.app.ws.WsCallback;
import id.uwinfly.app.ws.WsResponse;
import id.uwinfly.app.ws.interfaces.ReportWsIface;

public class ReportActivity extends BaseActivity {

    private final ReportItemAdapter reportAdapter = new ReportItemAdapter();
    private ReportWsIface reportWs;
    private Spinner spYearMonth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        reportWs = createWsCall(ReportWsIface.class);

        progressBar = findViewById(R.id.pb_list_report);

        button = findViewById(R.id.btn_filter_report);
        button.setOnClickListener(l -> filterSubmit());

        spYearMonth = findViewById(R.id.sp_year_month_report);
        ArrayAdapter adapter = ArrayAdapter
                .createFromResource(this, R.array.year_month, R.layout.item_remark);
        adapter.setDropDownViewResource(R.layout.item_dropdown_remark);
        spYearMonth.setAdapter(adapter);

        RecyclerView rvReport = findViewById(R.id.rv_list_report);
        rvReport.setAdapter(reportAdapter);

        firstLoad();
    }

    WsCallback<WsResponse<List<Report>>> listWsCallback = new WsCallback<WsResponse<List<Report>>>(this) {
        @Override
        public void onSuccess(WsResponse<List<Report>> response) {
            reportAdapter.setReports(response.getData());
        }
    };

    void filterSubmit() {
        loading(true);
        if(spYearMonth.getSelectedItemPosition() == 0) {
            showToast("Please Select Year Month", Toast.LENGTH_SHORT);
            loading(false);
        } else filter(spYearMonth.getSelectedItem().toString());
    }

    void firstLoad() {
        loading(true);
        reportWs.get().enqueue(listWsCallback);
    }

    void filter(String yearMonth) {
        reportWs.get(yearMonth).enqueue(listWsCallback);
    }
}
