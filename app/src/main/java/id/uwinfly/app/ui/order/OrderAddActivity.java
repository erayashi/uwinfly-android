package id.uwinfly.app.ui.order;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.button.MaterialButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.uwinfly.app.R;
import id.uwinfly.app.models.Customer;
import id.uwinfly.app.models.Order;
import id.uwinfly.app.models.OrderProduct;
import id.uwinfly.app.models.Product;
import id.uwinfly.app.models.Sales;
import id.uwinfly.app.ui.BaseActivity;
import id.uwinfly.app.ui.customer.CustomerActivity;
import id.uwinfly.app.ui.product.ProductActivity;
import id.uwinfly.app.ui.sales.SalesActivity;
import id.uwinfly.app.ws.WsCallback;
import id.uwinfly.app.ws.WsResponse;
import id.uwinfly.app.ws.interfaces.OrderWsIface;

public class OrderAddActivity extends BaseActivity implements OrderAddCallback {

    public static final String FROM_ORDER = "from_order";
    public static final String TO_ORDER = "to_order";
    private final OrderAddProductAdapter productAdapter = new OrderAddProductAdapter(this);
    private OrderWsIface orderIface;

    CardView cvCustomer, cvSales;
    TextView tvCustomer, tvSales, tvEmptyRv;
    MaterialButton btnAddProduct;
    RecyclerView rvProduct;

    Spinner spRemark;
    Customer customer;
    Sales sales;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_add);

        orderIface = createWsCall(OrderWsIface.class);
        cvCustomer = findViewById(R.id.select_customer_order);
        cvSales = findViewById(R.id.select_sales_order);
        tvCustomer = findViewById(R.id.tv_customer_order);
        tvSales = findViewById(R.id.tv_sales_order);
        tvEmptyRv = findViewById(R.id.tv_empty_product_order);
        rvProduct = findViewById(R.id.rv_product_order);
        spRemark = findViewById(R.id.sp_remark_order);
        btnAddProduct = findViewById(R.id.btn_add_product_order);
        button = findViewById(R.id.btn_add_order_order);
        progressBar = findViewById(R.id.pb_add_order);

        ArrayAdapter adapter = ArrayAdapter
                .createFromResource(this, R.array.remark, R.layout.item_remark);
        adapter.setDropDownViewResource(R.layout.item_dropdown_remark);
        spRemark.setAdapter(adapter);

        rvProduct.setAdapter(productAdapter);

        cvSales.setOnClickListener(l -> startActivityForResult(
                new Intent(this, SalesActivity.class),
                SalesActivity.WITH_RETURN_SALES
        ));

        cvCustomer.setOnClickListener(l -> startActivityForResult(
                new Intent(this, CustomerActivity.class),
                CustomerActivity.WITH_RETURN_CUSTOMER
        ));

        btnAddProduct.setOnClickListener(l -> startActivityForResult(
                new Intent(this, ProductActivity.class),
                ProductActivity.WITH_RETURN_PRODUCT
        ));

        button.setOnClickListener(l -> submitOrder());
    }

    void addProduct(Product product) {
        productAdapter.addProduct(product);
        showListProduct();
    }

    void showListProduct() {
        boolean displayProducts = productAdapter.getItemCount() > 0;
        rvProduct.setVisibility(displayProducts ? View.VISIBLE : View.GONE);
        tvEmptyRv.setVisibility(displayProducts ? View.GONE : View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(resultCode == RESULT_OK && data != null) {
            switch (requestCode) {
                case CustomerActivity.WITH_RETURN_CUSTOMER:
                    customer = data.getParcelableExtra(TO_ORDER);
                    tvCustomer.setText(customer.getName());
                    break;
                case SalesActivity.WITH_RETURN_SALES:
                    sales = data.getParcelableExtra(TO_ORDER);
                    tvSales.setText(sales.getName());
                    break;
                case ProductActivity.WITH_RETURN_PRODUCT:
                    Product product = data.getParcelableExtra(TO_ORDER);
                    if(productAdapter.isAlreadyAdd(product.getId()))
                        showToast("Product is Already added", Toast.LENGTH_LONG);
                    else addProduct(data.getParcelableExtra(TO_ORDER));
                    break;
                default:
            }
        }
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        intent.putExtra(FROM_ORDER, requestCode);
        super.startActivityForResult(intent, requestCode);
    }

    @Override
    public void notifyWhenRemoved() {
        showListProduct();
    }

    public void submitOrder() {
        loading(true);
        boolean processCreate = true;

        if(sales == null) {
            showToast("Please Select Sales", Toast.LENGTH_SHORT);
            processCreate = false;
        }

        if(customer == null && processCreate) {
            showToast("Please Select Customer", Toast.LENGTH_SHORT);
            processCreate = false;
        }

        if(spRemark.getSelectedItemPosition() == 0 && processCreate) {
            showToast("Please Select Remark", Toast.LENGTH_SHORT);
            processCreate = false;
        }

        if (productAdapter.getItemCount() < 1 && processCreate) {
            showToast("Please Add at Least One Product", Toast.LENGTH_SHORT);
            processCreate = false;
        }

        if(processCreate) {
            orderIface.create(makeOrder()).enqueue(
                    new WsCallback<WsResponse<Order>>(this) {
                @Override
                public void onSuccess(WsResponse<Order> response) {
                    loading(false);
                    showToast("Order is Added", Toast.LENGTH_LONG);
                    finish();
                }
            });
        } else loading(false);

    }

    private Order makeOrder() {
        Order order = new Order();
        order.setCustomerId(customer.getId());
        order.setSalesId(sales.getId());
        order.setRemark(spRemark.getSelectedItem().toString());

        List<OrderProduct> orderProducts = new ArrayList<>();

        for (Product product: productAdapter.getProducts()) {
            OrderProduct orderProduct = new OrderProduct();
            orderProduct.setProductId(product.getId());
            orderProduct.setQuantity(product.getQuantity());
            orderProducts.add(orderProduct);
        }

        order.setProducts(orderProducts);

        return order;
    }
}
