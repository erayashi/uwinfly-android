package id.uwinfly.app.ui.sales;

import android.content.Intent;
import android.view.View;

import id.uwinfly.app.models.Sales;
import id.uwinfly.app.ui.ListActivity;
import id.uwinfly.app.ws.WsCallback;
import id.uwinfly.app.ws.WsResponsePage;
import id.uwinfly.app.ws.interfaces.SalesWsIface;

public class SalesActivity extends ListActivity<Sales> {

    public static final int WITH_RETURN_SALES = 102;

    private final SalesItemAdapter sia = new SalesItemAdapter(this);
    private SalesWsIface salesWs;
    private final WsCallback<WsResponsePage<Sales>> callback =
            new WsCallback<WsResponsePage<Sales>>(this) {
                @Override
                public void onSuccess(WsResponsePage<Sales> response) {
                    if(response.getData() != null) sia.setSalesList(response.getData());
                }
            };

    @Override
    protected void whileCreate() {
        salesWs = createWsCall(SalesWsIface.class);
        getDataFromNetwork();
        setRecyclerViewAdapter(sia);
    }

    @Override
    protected void onFabClick(View v) {
        startActivity(new Intent(this, SalesAddActivity.class));
    }

    @Override
    protected void onSearchSubmit(String s) {
        if(s.isEmpty()) getDataFromNetwork();
        else salesWs.search(s).enqueue(callback);
    }

    private void getDataFromNetwork() {
        salesWs.gets().enqueue(callback);
    }

    @Override
    public void onItemClick(Sales item) {
        sendAddOrderResult(WITH_RETURN_SALES, item);
    }
}
