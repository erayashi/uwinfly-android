package id.uwinfly.app.ui.order;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.uwinfly.app.R;
import id.uwinfly.app.models.Order;
import id.uwinfly.app.models.OrderProduct;

public class OrderItemAdapter extends RecyclerView.Adapter<OrderItemAdapter.OrderViewHolder> {

    private List<Order> orders = new ArrayList<>();

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v = inflater.inflate(R.layout.item_order, viewGroup, false);
        return new OrderViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder orderViewHolder, int i) {
        Order order = orders.get(i);
        if(order != null) orderViewHolder.setWithOrder(order);
    }

    @Override
    public int getItemCount() {
        if(orders != null) return orders.size();
        return 0;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
        notifyDataSetChanged();
    }

    class OrderViewHolder extends RecyclerView.ViewHolder {
        TextView tvSales, tvCustomer, tvPrice, tvQty;
        String priceFormat, stockFormat;
        OrderViewHolder(@NonNull View itemView) {
            super(itemView);
            tvSales = itemView.findViewById(R.id.tv_sales_item_order);
            tvCustomer = itemView.findViewById(R.id.tv_customer_item_order);
            tvPrice = itemView.findViewById(R.id.tv_price_item_order);
            tvQty = itemView.findViewById(R.id.tv_qty_item_order);

            priceFormat = itemView.getContext().getString(R.string.price_format);
            stockFormat = itemView.getContext().getString(R.string.stock_format);
        }

        void setWithOrder(Order order) {
            int qty = 0;
            double price = 0;
            for (OrderProduct orderProduct: order.getProducts()) {
                qty += orderProduct.getQuantity();
                price += orderProduct.getPrice();
            }

            tvSales.setText(order.getSalesName());
            tvCustomer.setText(order.getCustomerName());
            tvPrice.setText(String.format(priceFormat, price));
            tvQty.setText(String.format(stockFormat, qty));
        }
    }
}
