package id.uwinfly.app.ui.sales;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.uwinfly.app.R;
import id.uwinfly.app.models.Sales;
import id.uwinfly.app.ui.ItemCallback;

public class SalesItemAdapter extends RecyclerView.Adapter<SalesItemAdapter.SalesItemViewHolder> {

    private List<Sales> salesList = new ArrayList<>();
    private ItemCallback<Sales> itemCallback;

    SalesItemAdapter(ItemCallback<Sales> itemCallback) {
        this.itemCallback = itemCallback;
    }

    @NonNull
    @Override
    public SalesItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v = inflater.inflate(R.layout.item_sales, viewGroup, false);
        return new SalesItemViewHolder(v, itemCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull SalesItemViewHolder salesItemViewHolder, int i) {
        Sales sales = salesList.get(i);
        if(sales != null) salesItemViewHolder.setWithSales(sales);
    }

    @Override
    public int getItemCount() {
        if(salesList == null) return 0;
        return salesList.size();
    }

    void setSalesList(List<Sales> salesList) {
        this.salesList = salesList;
        notifyDataSetChanged();
    }

    class SalesItemViewHolder extends RecyclerView.ViewHolder {
        TextView name, address, phone;
        Sales mSales;

        SalesItemViewHolder(@NonNull View itemView, ItemCallback<Sales> itemCallback) {
            super(itemView);
            name = itemView.findViewById(R.id.isales_name);
            address = itemView.findViewById(R.id.isales_address);
            phone = itemView.findViewById(R.id.isales_phone);

            itemView.setOnClickListener(l -> itemCallback.onItemClick(mSales));
        }

        void setWithSales(Sales sales) {
            mSales = sales;
            name.setText(sales.getName());
            address.setText(sales.getAddress());
            phone.setText(sales.getPhoneNumber());
        }
    }
}
